obj-m := led_driver.o
KDIR := /home/feather973/rpi_4.19/linux
OUTDIR := /home/feather973/rpi_4.19/out
HDRDIR := /home/feather973/rpi_4.19/gpio_driver2/header

defualt:
	make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- INSTALL_HDR_PATH=$(HDRDIR) -C$(KDIR) headers_install
	make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- -I$(HDRDIR)/include -I$(OUTDIR)/include -C$(OUTDIR) M=$(shell pwd) modules
#	make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- -C$(OUTDIR) M=$(shell pwd) modules
	arm-linux-gnueabihf-gcc -o led_app led_app.c

clean:
	make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- -C$(OUTDIR) M=$(shell pwd) clean
