#define DEVICE_NAME     "mydriver"   
#define LED_MAGIC       (123)
#define LED_NR          (6)

#define IOCTL_LED_1_ON      _IO(LED_MAGIC, 0)
#define IOCTL_LED_2_ON      _IO(LED_MAGIC, 1)
#define IOCTL_LED_3_ON      _IO(LED_MAGIC, 2)
#define IOCTL_LED_4_ON      _IO(LED_MAGIC, 3)
#define IOCTL_LED_BLINK		_IO(LED_MAGIC, 4)
#define IOCTL_LED_RESET		_IO(LED_MAGIC, 5)

// check already used pins
// https://elinux.org/RPi_BCM2835_GPIOs
#define LED1            (17)
#define LED2            (13)
#define LED3            (24)
#define LED4            (22)
