#include <stdio.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <stdlib.h>
#include <unistd.h>

#include "led_ioctl.h"

int main(int argc, char **argv)
{
	int cmd = atoi(argv[1]);

	int fd = open("/dev/mydriver", O_RDWR);
	if(fd < 0){
		printf("/dev/mydriver open fail\n");
		return -1;
	}
	
	switch(cmd)
	{
		case 1:
			ioctl(fd, IOCTL_LED_1_ON, NULL);
			break;
		case 2:
			ioctl(fd, IOCTL_LED_2_ON, NULL);
			break;
		case 3:
			ioctl(fd, IOCTL_LED_3_ON, NULL);
			break;
		case 4:
			ioctl(fd, IOCTL_LED_4_ON, NULL);
			break;
		case 5:
			ioctl(fd, IOCTL_LED_BLINK, NULL);
			break;
		case 6:
			ioctl(fd, IOCTL_LED_RESET, NULL);
			break;
		default:
			printf("unknown ioctl\n");			
			break;
	}

	return 0;	
}
