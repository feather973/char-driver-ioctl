/* https://webnautes.tistory.com/754 */
#include <linux/module.h>    
#include <linux/kernel.h>    
#include <linux/cdev.h>    
#include <linux/device.h>    
#include <linux/fs.h>              
#include <linux/slab.h>    
//#include <asm/uaccess.h>    
#include <linux/uaccess.h>
#include <linux/gpio.h>
#include <linux/delay.h>
        
dev_t id;    
struct cdev cdev;    
struct class *class;    
struct device *dev;    
        
char temp[100];    
        
#include "led_ioctl.h"

void blink(void)
{
	int i;

	for(i=0;i<3;i++)
	{
		gpio_set_value(LED1, 0);
		gpio_set_value(LED2, 1);
		gpio_set_value(LED3, 0);
		gpio_set_value(LED4, 1);
		msleep(1000);

		gpio_set_value(LED1, 1);
		gpio_set_value(LED2, 0);
		gpio_set_value(LED3, 1);
		gpio_set_value(LED4, 0);
		msleep(1000);
	}

	gpio_set_value(LED1, 0);
	gpio_set_value(LED2, 0);
	gpio_set_value(LED3, 0);
	gpio_set_value(LED4, 0);
}

int led_open (struct inode *inode, struct file *filp)    
{    
    printk( "open\n" );    
    memset( temp, 0, 0 );    
        
    return 0;    
}    
    
int led_close (struct inode *inode, struct file *filp)    
{    
    printk( "close\n" );    
    return 0;    
}    
        
ssize_t led_read(struct file *filp, char *buf, size_t size, loff_t *offset)    
{    
	int ret;
    printk( "led_read\n" );    
    printk( "DEV : write something\n" );    
    printk( "%s %dbytes\n", temp, strlen(temp) );    
    ret = copy_to_user( buf, temp, strlen(temp)+1 );    
        
    return strlen(temp);    
}    
        
ssize_t led_write (struct file *filp, const char *buf, size_t size, loff_t *offset)    
{    
	int ret;
    printk( "led_write\n" );    
    printk( "DEV : read something\n");    
        
    ret = copy_from_user( temp, buf, size );    
    printk( "%s %dbytes\n", temp, size );    
        
    return size;    
}    
        
long led_ioctl ( struct file *filp, unsigned int cmd, unsigned long arg)    
{    
    printk( "ioctl\n" );    

	if(_IOC_TYPE(cmd) != LED_MAGIC)		return -EINVAL;		// magic cheeck

	if(_IOC_NR(cmd) >= LED_NR)			return -EINVAL;		// cmd not exceed NR

	switch(cmd){
		case IOCTL_LED_1_ON:
			gpio_set_value(LED1, 1);		break;
		case IOCTL_LED_2_ON:
			gpio_set_value(LED2, 1);		break;
		case IOCTL_LED_3_ON:
			gpio_set_value(LED3, 1);		break;
		case IOCTL_LED_4_ON:
			gpio_set_value(LED4, 1);		break;
		case IOCTL_LED_BLINK:
			blink();						break;
		case IOCTL_LED_RESET:
			gpio_set_value(LED1, 0);		
			gpio_set_value(LED2, 0);		
			gpio_set_value(LED3, 0);		
			gpio_set_value(LED4, 0);		break;
		default:
			printk("undefined ioctl\n");	break;
	}

    return 0;    
}    
        
struct file_operations led_fops =    
{    
    .owner           = THIS_MODULE,    
    .read            = led_read,         
    .write           = led_write,        
    .unlocked_ioctl  = led_ioctl,        
    .open            = led_open,         
    .release         = led_close,      
};    
        
int led_init(void)    
{    
    int ret;    
        
    ret = alloc_chrdev_region( &id, 0, 1, DEVICE_NAME );    
    if ( ret ){    
        printk( "alloc_chrdev_region error %d\n", ret );    
        return ret;    
    }    
        
    cdev_init( &cdev, &led_fops );    
    cdev.owner = THIS_MODULE;    
        
    ret = cdev_add( &cdev, id, 1 );    
    if (ret){    
        printk( "cdev_add error %d\n", ret );    
        unregister_chrdev_region( id, 1 );    
        return ret;    
    }    
        
    class = class_create( THIS_MODULE, DEVICE_NAME );    
    if ( IS_ERR(class)){    
        ret = PTR_ERR( class );    
        printk( "class_create error %d\n", ret );    
        
        cdev_del( &cdev );    
        unregister_chrdev_region( id, 1 );    
        return ret;    
    }    
        
    dev = device_create( class, NULL, id, NULL, DEVICE_NAME );    
    if ( IS_ERR(dev) ){    
        ret = PTR_ERR(dev);    
        printk( "device_create error %d\n", ret );    
        
        class_destroy(class);    
        cdev_del( &cdev );    
        unregister_chrdev_region( id, 1 );    
        return ret;    
    }    

	/* LED GPIO INIT */
    ret = gpio_request_one(LED1, GPIOF_OUT_INIT_LOW, "LED PIN 1");
    if (ret < 0){ 
        pr_err("[%d] %s\n", 1, __func__);
        return -1; 
    }
	gpio_export(LED1, true);

    ret = gpio_request_one(LED2, GPIOF_OUT_INIT_LOW, "LED PIN 2");
    if (ret < 0){ 
        pr_err("[%d] %s\n", 2, __func__);
        return -1; 
    }
	gpio_export(LED2, true);

    ret = gpio_request_one(LED3, GPIOF_OUT_INIT_LOW, "LED PIN 3");
    if (ret < 0){ 
		printk("error : %d\n", ret);
        pr_err("[%d] %s\n", 3, __func__);
        return -1; 
    }
	gpio_export(LED3, true);

    ret = gpio_request_one(LED4, GPIOF_OUT_INIT_LOW, "LED PIN 4");
    if (ret < 0){ 
		printk("error : %d\n", ret);
        pr_err("[%d] %s\n", 4, __func__);
        return -1; 
    }
	gpio_export(LED4, true);
        
    return 0;    
}    
        
    
void led_exit(void)    
{    
    device_destroy(class, id );    
    class_destroy(class);    
    cdev_del( &cdev );    
    unregister_chrdev_region( id, 1 );    

	/* LED GPIO EXIT */
	gpio_free(LED1);
	gpio_free(LED2);
	gpio_free(LED3);
	gpio_free(LED4);
}    
    
        
module_init(led_init);    
module_exit(led_exit);  
    
MODULE_LICENSE("Dual BSD/GPL");   
