#include <linux/build-salt.h>
#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

BUILD_SALT;

MODULE_INFO(vermagic, VERMAGIC_STRING);
MODULE_INFO(name, KBUILD_MODNAME);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

#ifdef CONFIG_RETPOLINE
MODULE_INFO(retpoline, "Y");
#endif

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x9b929f65, "module_layout" },
	{ 0xfe990052, "gpio_free" },
	{ 0x68d910a3, "device_destroy" },
	{ 0x37c80b68, "class_destroy" },
	{ 0x6091b333, "unregister_chrdev_region" },
	{ 0xd6134b38, "cdev_del" },
	{ 0x529e83dd, "gpiod_export" },
	{ 0x403f9529, "gpio_request_one" },
	{ 0x2bdad51a, "device_create" },
	{ 0x5784d85c, "__class_create" },
	{ 0xaa3f5a40, "cdev_add" },
	{ 0x1ea57847, "cdev_init" },
	{ 0xe3ec2f2b, "alloc_chrdev_region" },
	{ 0xf9a482f9, "msleep" },
	{ 0x9a8d7dc4, "gpiod_set_raw_value" },
	{ 0x9b688965, "gpio_to_desc" },
	{ 0x5f754e5a, "memset" },
	{ 0x28cc25db, "arm_copy_from_user" },
	{ 0x1e047854, "warn_slowpath_fmt" },
	{ 0xf4fa543b, "arm_copy_to_user" },
	{ 0x97255bdf, "strlen" },
	{ 0x2e5810c6, "__aeabi_unwind_cpp_pr1" },
	{ 0x7c32d0f0, "printk" },
	{ 0xb1ad28e0, "__gnu_mcount_nc" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";


MODULE_INFO(srcversion, "ED40D6AE8E1CCECD4EE2B7A");
